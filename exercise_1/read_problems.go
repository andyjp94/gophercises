package main

import (
	"encoding/csv"
	"io"
	"os"
)

func readProblems(f string) ([]problem, error) {
	file, err := os.Open(f) // For read access.
	defer file.Close()
	if err != nil {
		return nil, err
	}

	var problems []problem
	r := csv.NewReader(file)
	for {
		record, err := r.Read()
		if err == io.EOF {
			return problems, nil
		}
		if err != nil {
			return problems, err
		}

		problems = append(problems, problem{question: record[0], answer: record[1]})
	}
}
