package main

import (
	"testing"
)

func equal(a, b []problem) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func checkProblems(t *testing.T, problems []problem) {
	b := [13]problem{
		problem{question: "5+5", answer: "10"},
		problem{question: "7+3", answer: "10"},
		problem{question: "1+1", answer: "2"},
		problem{question: "8+3", answer: "11"},
		problem{question: "1+2", answer: "3"},
		problem{question: "8+6", answer: "14"},
		problem{question: "3+1", answer: "4"},
		problem{question: "1+4", answer: "5"},
		problem{question: "5+1", answer: "6"},
		problem{question: "2+3", answer: "5"},
		problem{question: "2+3", answer: "6"},
		problem{question: "2+4", answer: "6"},
		problem{question: "5+2", answer: "7"},
	}

	if equal(b[:], problems) {
		t.Error("The CSV file does not match the expected input.")
	}

}

func TestProblemReaderiFileDoesNotExist(t *testing.T) {
	_, err := readProblems("asdf.csv")

	if err == nil {
		t.Errorf("No error returned when a non existent problem file is selected.")
	}
}

func TestProblemReaderInvalidCSV(t *testing.T) {
	_, err := readProblems("testdata/invalid.csv")

	if err == nil {
		t.Errorf("No error returned when using an invalid CSV")
	}
}

func TestProblemReaderDefaultFile(t *testing.T) {
	problems, err := readProblems("testdata/problems.csv")
	if err != nil {
		t.Errorf("Error returned.")
	}
	checkProblems(t, problems)

}

func TestProblemReaderCustomFile(t *testing.T) {
	problems, err := readProblems("testdata/not_called_problems.csv")
	if err != nil {
		t.Errorf("Error returned.")
	}

	checkProblems(t, problems)
}
