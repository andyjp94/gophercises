package main

import (
	"bytes"
	"fmt"
	"testing"
)

func answer(q, a string, correct bool) problem {
	var stdin bytes.Buffer
	p := problem{question: q, answer: a}
	var answer []byte
	if correct == true {
		answer = []byte(fmt.Sprintf("%s\n", a))
	} else {
		answer = []byte("\n")
	}

	stdin.Write(answer)
	p.ask(&stdin)
	return p
}

func answerAll(t *testing.T, correct bool) {
	preset := !correct
	b := [13]problem{
		problem{question: "5+5", answer: "10", correct: preset},
		problem{question: "7+3", answer: "10", correct: preset},
		problem{question: "1+1", answer: "2", correct: preset},
		problem{question: "8+3", answer: "11", correct: preset},
		problem{question: "1+2", answer: "3", correct: preset},
		problem{question: "8+6", answer: "14", correct: preset},
		problem{question: "3+1", answer: "4", correct: preset},
		problem{question: "1+4", answer: "5", correct: preset},
		problem{question: "5+1", answer: "6", correct: preset},
		problem{question: "2+3", answer: "5", correct: preset},
		problem{question: "2+3", answer: "6", correct: preset},
		problem{question: "2+4", answer: "6", correct: preset},
		problem{question: "5+2", answer: "7", correct: preset},
	}
	var stdin bytes.Buffer
	var answer []byte
	if correct == true {
		answer = []byte("10\n10\n2\n11\n3\n14\n4\n5\n6\n5\n6\n6\n7\n")
	} else {
		answer = []byte("a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\n\nl\n")
	}

	stdin.Write(answer)
	problems, err := askAll(b[:], &stdin)
	if err != nil {
		t.Error(err)
	}
	for _, problems := range problems {
		if problems.correct != correct {
			t.Errorf("Gave the %t answer to %s and the result was %t", correct, problems.question, problems.correct)
		}
	}

}

// func TestAskAllRight(t *testing.T) {
// 	answerAll(t, true)
// }

// func TestAskAllWrong(t *testing.T) {
// 	answerAll(t, false)
// }

func TestProblemAskRight(t *testing.T) {
	p := answer("a", "b", true)
	if p.correct != true {
		t.Errorf("Input %q did not match the expected answer of %s.\n", "b", p.answer)
	}
}

func TestProblemAskWrong(t *testing.T) {
	p := answer("a", "b", false)
	if p.correct == true {
		t.Errorf("Input %q matched the expected answer of %s.\n", "", p.answer)
	}
}
