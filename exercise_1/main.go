package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

type problem struct {
	question, answer string
	correct          bool
}

func (p *problem) ask(stdin io.Reader) {
	fmt.Printf("What is %s?\n", p.question)
	scanner := bufio.NewScanner(stdin)
	scanner.Scan()

	if scanner.Text() == p.answer {
		p.correct = true
	}
}

func askAll(problems []problem, stdin io.Reader) ([]problem, error) {
	attempts := make([]problem, len(problems))
	for i, p := range problems {
		p.ask(stdin)
		attempts[i] = p
	}
	return attempts, nil
}

func main() {
	problems, err := readProblems("testdata/problems.csv")
	if err != nil {
		os.Exit(1)
	}
	p, err := askAll(problems, os.Stdin)
	if err != nil {
		os.Exit(1)
	}

	fmt.Print(p)
}
